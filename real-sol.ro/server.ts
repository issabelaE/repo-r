import 'zone.js/dist/zone-node';
import {enableProdMode} from '@angular/core';
// Express Engine
import {ngExpressEngine} from '@nguniversal/express-engine';
// Import module map for lazy loading
import {provideModuleMap} from '@nguniversal/module-map-ngfactory-loader';

import * as express from 'express';
import * as helmet from 'helmet';
import {join} from 'path';

// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();

// Express server
const app = express();
app.use(helmet());

const PORT = process.env.PORT || 4200;
const SERVER_TEST = process.env.SERVER_TEST || '0';
const DIST_FOLDER = join(process.cwd(), 'dist/browser');

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
/* tslint:disable:no-var-requires */
const { AppServerModuleNgFactory, LAZY_MODULE_MAP } = require('./dist/server/main');
/* tslint:enable */

// Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
app.engine('html', ngExpressEngine({
  bootstrap: AppServerModuleNgFactory,
  providers: [
    provideModuleMap(LAZY_MODULE_MAP)
  ]
}));

app.set('view engine', 'html');
app.set('views', DIST_FOLDER);
app.disable('x-powered-by');

// global controller
app.get('/*', (req, res, next) => {
    if (process.env.APP_RELEASE) {
      res.header('app-release', process.env.APP_RELEASE);
    } else {
      console.error('APP_RELEASE not set.');
    }
    next(); // http://expressjs.com/guide.html#passing-route control
});
app.get(
  '/*.(css|css.map|js|js.map|woff|woff2|ttf|eot)?/',
  express.static(
    join(DIST_FOLDER),
    {
      maxage: '1y',
      immutable: true
    }
  )
);
// Example Express Rest API endpoints
// app.get('/api/**', (req, res) => { });
// Serve static files from /browser
app.get(
  '*.*',
  (req, res) => {
    const path = req.params[0] + '.' + req.params[1];
    let maxage = '30m';
    let immutable = false;

    if (process.env.APP_RELEASE === req.query.v) {
      maxage = '1y';
      immutable = true;
    }

    res.sendFile(
      path,
      {
        root: join(DIST_FOLDER),
        maxage: maxage,
        immutable: immutable
      }
    );
  }
);

// All regular routes use the Universal engine
app.get('*', (req, res) => {
  res.render('index', { req });
});

// Start up the Node server
if (!parseInt(SERVER_TEST)) {
  /* tslint:disable:no-console */
  app.listen(PORT, () => {
    console.log('App release: ' + process.env.APP_RELEASE);
    console.log(`Listening on http://localhost:${PORT}...`);
  });
  /* tslint:enable */
}
