import { Component, Injector, OnInit, OnDestroy } from '@angular/core';
import { SeoAwareBaseComponent } from '../../common-theme/seo/seo-aware-base.component';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Block } from './../../common-theme/models/index';
import { BlockHelperService } from './../../common-theme/index';

@Component({
  selector: 'app-project1',
  templateUrl: './project1.component.html',
  styleUrls: ['./project1.component.scss']
})
export class Project1Component extends SeoAwareBaseComponent implements OnInit, OnDestroy {
  subscription: Subscription = new Subscription();
  public blocks: Block[];
  sliders: any[];

  constructor(
    public blockHelper: BlockHelperService,
    public route: ActivatedRoute,
    public injector: Injector
  ) {
    super (injector);
  }

  ngOnInit(): void {
    this.subscription.add(this.route.data.subscribe(data => {
      this.blocks = data.blocks;
      this.setTitle(this.blockHelper.filterBlocksByKey(this.blocks, 'seo').properties.title);
      this.setMetaDescription(this.blockHelper.filterBlocksByKey(this.blocks, 'seo').properties.description);

      this.sliders = this.blocks.filter(obj => {
        return obj.key.startsWith('slider');
      });
    }));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
