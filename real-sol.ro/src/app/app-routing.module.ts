import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BlocksService } from './common-theme/api/index';
import { HomeComponent } from './home/home.component';
import { OurStoryComponent } from './our-story/our-story.component';
import { OurProjectsComponent } from './our-projects/our-projects.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {
      blocks_categories: ['homepage']
    },
    resolve: {
      blocks: BlocksService
    }
  },
  {
    path: ':locale',
    component: HomeComponent,
    data: {
      blocks_categories: ['homepage']
    },
    resolve: {
      blocks: BlocksService
    }
  },
  {
    path: ':locale/our-story',
    component: OurStoryComponent,
    data: {
      blocks_categories: ['our-story']
    },
    resolve: {
      blocks: BlocksService
    }
  },
  {
    path: ':locale/our-projects',
    component: OurProjectsComponent,
    data: {
      blocks_categories: ['our-projects']
    },
    resolve: {
      blocks: BlocksService
    }
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
