import * as Raven from 'raven-js';
import { environment } from './../../environments/environment';
import { ErrorHandler, Inject, Injectable } from '@angular/core';
import { ApiError, FormError } from './models/index';

Raven
  .config(environment.sentry_public_dsn)
  .install();

@Injectable()
export class RavenErrorHandler implements ErrorHandler {
  constructor(
    @Inject('WINDOW') private window: any
  ) { }

  handleError(error: any): void {
    if (error.originalError) {
      error = error.originalError;
    } else if (error.rejection) {
      error = error.rejection;
    }

    Raven.setTagsContext({
      website_uuid: environment.website_uuid
    });

    Raven.captureException(error);

    if (!environment.production) {
      console.error(error);
    } else if (!(error instanceof ApiError) && !(error instanceof FormError)) {
      if (this.window) {
        this.window.alert(
          'Ne cerem scuze pentru inconveniență.\n' +
          'Echipa a fost notificată de eroare și va remedia problema cât de repede posibil.'
        );
        this.window.location.reload();
      }
    }
  }
}
