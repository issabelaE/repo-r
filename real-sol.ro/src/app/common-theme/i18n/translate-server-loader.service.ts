import { Observable } from 'rxjs';
import { TranslateLoader } from '@ngx-translate/core';
import { makeStateKey, StateKey, TransferState } from '@angular/platform-browser';

declare var require: any;
declare var process: any;

// tslint:disable:no-var-requires
const fs = require('fs');
const path = require('path');
// tslint:enable

// tslint:disable:only-arrow-functions
export function translateFactory(transferState: TransferState): any {
  return new TranslateServerLoader('/assets/i18n', '.json', transferState);
}
// tslint:enable

export class TranslateServerLoader implements TranslateLoader {

  constructor(
    private prefix: string = 'i18n',
    private suffix: string = '.json',
    private transferState: TransferState) {
  }

  public getTranslation(lang: string): Observable<any> {

    return Observable.create(observer => {
      const assets_folder = path.join(process.cwd(), 'dist', 'server', this.prefix);

      const jsonData = JSON.parse(fs.readFileSync(`${assets_folder}/${lang}${this.suffix}`, 'utf8'));

      // Here we save the translations in the transfer-state
      const key: StateKey<number> = makeStateKey<number>('transfer-translate-' + lang);
      this.transferState.set(key, jsonData);

      observer.next(jsonData);
      observer.complete();
    });
  }
}
