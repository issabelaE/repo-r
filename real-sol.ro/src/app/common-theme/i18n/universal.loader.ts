import { isPlatformServer } from '@angular/common';
import { makeStateKey, StateKey, TransferState } from '@angular/platform-browser';
import { TranslateLoader } from '@ngx-translate/core';
import { Observable } from 'rxjs';

declare var require: any;
declare var process: any;

// tslint:disable:no-var-requires
const fs = require('fs');
const path = require('path');
// tslint:enable

// tslint:disable:only-arrow-functions
export function translateFactory(transferState: TransferState, plateformId: object): any {
  return new TranslateUniversalLoader(isPlatformServer(plateformId), '/assets/i18n/', '.json', transferState);
}
// tslint:enable

export class TranslateUniversalLoader implements TranslateLoader {

  constructor(
    private isServer: boolean,
    private prefix: string = 'i18n',
    private suffix: string = '.json',
    private transferState: TransferState) {
  }

  public getTranslation(lang: string): Observable<any> {
    const key: StateKey<number> = makeStateKey<number>('transfer-translate-' + lang);

    return Observable.create(observer => {
      if (this.isServer) {
        const assets_folder = path.join(process.cwd(), 'dist', 'server', this.prefix);

        const jsonData = JSON.parse(fs.readFileSync(`${assets_folder}${lang}${this.suffix}`, 'utf8'));

        this.transferState.set(key, jsonData);
      }

      observer.next(this.transferState.get(key, null));
      observer.complete();
    });
  }
}
