import { Injector, OnDestroy } from '@angular/core';
import { TranslateService, LangChangeEvent, DefaultLangChangeEvent } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { environment } from './../../../environments/environment';
import { Router } from '@angular/router';
import { DOCUMENT } from "@angular/common";

export abstract class LocaleAwareBaseComponent implements OnDestroy {
  static staticBasePath: string = '/';
  static staticLocale: string = 'ro';
  static pathPrefix: string = '/lang-';
  protected subscription: Subscription = new Subscription();

  get basePath(): string {
    return LocaleAwareBaseComponent.staticBasePath;
  }
  set basePath(basePath: string) {
    LocaleAwareBaseComponent.staticBasePath = basePath;
  }

  constructor(
    public injector?: Injector
  ) {
    if (!injector)
      return;

    const translate = this.injector.get(TranslateService);

    translate.setDefaultLang('ro');

    if (environment.router_link_has_locale === true) {
      this.setLocale(translate, this.getLocale());

      this.subscription.add(translate.onDefaultLangChange.subscribe((event: DefaultLangChangeEvent) => {
        this.basePath = LocaleAwareBaseComponent.pathPrefix + event.lang;
      }));
      this.subscription.add(translate.onLangChange.subscribe((event: LangChangeEvent) => {
        this.basePath = LocaleAwareBaseComponent.pathPrefix + event.lang;
      }));
    }
  }

  ngOnDestroy(): void {
    if (this.subscription)
      this.subscription.unsubscribe();
  }

  getLocalePathname(newLang: string): string|boolean {
    if (window) {
      let href = window.location.href;
      if (href.endsWith('/'))
        href = href.substring(0, href.length - 1);

      if (window.location.pathname.startsWith(this.basePath))
        return href.replace(this.basePath, LocaleAwareBaseComponent.pathPrefix + newLang);
      if (environment.router_link_has_locale === true) {
        return href + LocaleAwareBaseComponent.pathPrefix + newLang;
      }
    }
    return null;
  }

  getLocale(): string {
    const router = this.injector.get(Router);

    if (router.url.startsWith(LocaleAwareBaseComponent.pathPrefix + 'ro')) {
      return 'ro';
    } else if (router.url.startsWith(LocaleAwareBaseComponent.pathPrefix + 'en')) {
      return 'en';
    } else { // default landing page
      return 'ro';
    }
  }

  setLocale(translate: TranslateService, locale: string): void {
    translate.use(locale);
    LocaleAwareBaseComponent.staticLocale = locale;
    this.basePath = LocaleAwareBaseComponent.pathPrefix + locale;

    if (this.injector) {
      const document = this.injector.get(DOCUMENT);
      document.documentElement.lang = locale;
    }
  }
}
