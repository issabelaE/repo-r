import { HttpErrorResponse } from '@angular/common/http';

export class FormError extends Error {
  code: number;
  errors: any = [];
  api_raw_response: HttpErrorResponse;
}
