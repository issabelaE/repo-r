export class Block {
  id: number;
  key: string;
  image: any;
  icon: any;
  block_category: {
    key: string;
  }
  properties: any;
}
