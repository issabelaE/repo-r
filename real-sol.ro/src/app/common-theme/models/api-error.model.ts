import { HttpErrorResponse } from '@angular/common/http';

export class ApiError extends Error {
  code: number;
  message: string;
  errors: any = [];
  api_raw_response: HttpErrorResponse;
}
