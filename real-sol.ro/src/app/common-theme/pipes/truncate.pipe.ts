import { Pipe, PipeTransform } from '@angular/core';

/**
 * Truncate a string to the given length and append suffix.
 * @param	length Text max length. Default 20.
 * @param	suffix Appended to the end of the string if truncted. Default ''.
 * @example Usage:
 * ```html
 * <p>{{ 'Hello world' | truncate:5:'...' }}</p>
 * <!-- Formats to: 'Hello...' -->
 * ```
 */
@Pipe({name: 'truncate'})
export class TruncatePipe implements PipeTransform {
  transform(value: string, length: number, suffix: string): string {
    if (length === undefined) {
      length = 20;
    }

    if (suffix === undefined) {
      suffix = '...';
    }

    if (value.length <= length) {
      return value;
    }

    return value.substring(0, length) + suffix;
  }
}
