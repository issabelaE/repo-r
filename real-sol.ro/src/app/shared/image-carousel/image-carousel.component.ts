import { Component, OnInit, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-image-carousel',
  templateUrl: './image-carousel.component.html',
  styleUrls: ['./image-carousel.component.scss']
})
export class ImageCarouselComponent implements OnInit, OnDestroy {

  @Input() intervalToSlide: number = 5000;
  @Input() sliders: any;
  selectedIndex: number = 0;
  interval: any;

  ngOnInit(): void {
    this.initSlider();
  }

  initSlider(): void {
    this.interval = setInterval(() => {
      if (this.selectedIndex < this.sliders.length - 1) {
        this.selectedIndex++;
      } else {
        this.selectedIndex = 0;
      }
    },                          this.intervalToSlide);
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
  }

  prev(): void {
    if (this.selectedIndex > 0) {
      this.selectedIndex--;
    } else {
      this.selectedIndex = this.sliders.length - 1;
    }
  }

  next(): void {
    if (this.selectedIndex < this.sliders.length - 1) {
      this.selectedIndex++;
    } else {
      this.selectedIndex = 0;
    }
  }
}
