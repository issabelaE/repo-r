import { Component, OnInit } from '@angular/core';
import { LocaleAwareBaseComponent } from './../../common-theme/seo/locale-aware-base.component';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { WebsiteService } from './../../common-theme/api/website.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends LocaleAwareBaseComponent implements OnInit {
  menuIsOpen: boolean = false;
  lastScrollTop: number = 0;
  desktopShowNavMenu: boolean;
  navItems: any[] = [
    {
      name: 'Our Story',
      link: '/our-story'
    },
    {
      name: 'Our Services',
      link: '/our-services'
    },
    {
      name: 'Our Projects',
      link: '/our-projects'
    },
    {
      name: 'News',
      link: '/news'
    }
  ];

  constructor(
    public translate: TranslateService,
    public router: Router,
    public websiteService: WebsiteService
  ) {
    super();
    this.router.events.subscribe(() => {
      this.menuIsOpen = false;
      document.body.classList.remove('no-scroll');
    });
  }

  ngOnInit(): void {
    this.desktopShowNavMenu = window.innerWidth >= 960;
  }

  toggleMenu(): void {
    if (window.innerWidth < 960) {
      this.menuIsOpen = !this.menuIsOpen;
      this.menuIsOpen ? document.body.classList.add('no-scroll') : document.body.classList.remove('no-scroll');
    }
  }

  onResize(): void {
    if (window.innerWidth >= 960) {
      this.menuIsOpen = false;
      this.desktopShowNavMenu = true;
      document.body.classList.remove('no-scroll');
    } else {
      this.desktopShowNavMenu = false;
    }
  }

  onScroll(): void {
    const scrollToTop = document.getElementById('scroll_to_top');
    if (window.scrollY > 500)
      scrollToTop.classList.add('show');
    else
      scrollToTop.classList.remove('show');
  }
}
