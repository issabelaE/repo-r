export const environment = {
  production: true,
  apiUrl: 'https://vaa.aesthetic-works.net',
  baseApiPath: 'v1/site',
  agency_uuid: '2b717356-c4b2-4546-9aff-683ab784aeef',
  sentry_public_dsn: 'https://a23296e28abd4019bb288d7acef1ba8a@sentry.aesthetic-works.net/49',
  re_captcha_site_key: '6LcwLwkUAAAAAH1uYGLTrKB7NgeTqrLF5SDMw3-j',
  ads_tracking_id: null,
  analytics_tracking_id: null,
  facebook_pixel_id: null,
  release: 'staging',
  router_link_has_locale: true,
  ads_phone_conversion_label: null
};
