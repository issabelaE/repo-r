// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://cms-api.aesthetic-works.net',
  baseApiPath: 'v1/site',
  website_uuid: 'cd88444f-f53f-4101-a55a-49a03444ba83',
  sentry_public_dsn: null,
  re_captcha_site_key: '6LfqrKkUAAAAAL7p3wmUJpp4YWjnLLQYafCkzT1w',
  ads_tracking_id: null,
  analytics_tracking_id: null,
  facebook_pixel_id: null,
  release: null,
  router_link_has_locale: true,
  ads_phone_conversion_label: null
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
