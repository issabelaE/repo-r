export const environment = {
  production: false,
  apiUrl: 'https://cms-api.aesthetic-works.net',
  baseApiPath: 'v1/site',
  website_uuid: '09340c73-0917-44fe-a0e2-f2775958c20b',
  sentry_public_dsn: null,
  re_captcha_site_key: '6LcwLwkUAAAAAH1uYGLTrKB7NgeTqrLF5SDMw3-j',
  ads_tracking_id: null,
  analytics_tracking_id: null,
  facebook_pixel_id: null,
  release: 'dev',
  router_link_has_locale: true,
  ads_phone_conversion_label: null
};
